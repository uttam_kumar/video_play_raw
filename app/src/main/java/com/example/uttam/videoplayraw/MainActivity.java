package com.example.uttam.videoplayraw;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class MainActivity extends Activity {

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mp = MediaPlayer.create(this, R.raw.water_rapids);

        SurfaceView sv1 = (SurfaceView) findViewById(R.id.surfaceView1);
        SurfaceHolder holder1 = sv1.getHolder();
        holder1.addCallback(new Callback(){
            @Override
            public void surfaceChanged(SurfaceHolder holder1, int format, int width, int height) { }

            @Override
            public void surfaceCreated(SurfaceHolder holder1) {
                mp.setDisplay(holder1);
                mp.start();
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder1) { }
        });



        SurfaceView sv2 = (SurfaceView) findViewById(R.id.surfaceView2);
        SurfaceHolder holder2 = sv2.getHolder();
        holder2.addCallback(new Callback(){
            @Override
            public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) { }

            @Override
            public void surfaceCreated(SurfaceHolder holder2) {
                mp.setDisplay(holder2);
                mp.start();
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder2) { }
        });

    }

    @Override
    protected void onPause(){
        super.onPause();

        if(null != mp) {
            mp.release();
            mp = null;
        }
    }
}